package com.femonres.financial.domain.services;

import com.femonres.financial.domain.entities.Account;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface AccountService {

    Account openAccount(@NotNull String clientId, Account account);

    Optional<Account> getOne(@NotNull String accountId);

    void closeAccount(@NotNull String accountId);

    void addMovement(@NotNull String accountId, @NotNull Double amount);
}
