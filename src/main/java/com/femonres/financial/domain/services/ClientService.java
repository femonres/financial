package com.femonres.financial.domain.services;

import com.femonres.financial.domain.entities.Client;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface ClientService {
    List<Client> getAll();

    Optional<Client> getOne(@NotNull String id);

    Client create(@NotNull Client client);

    Client update(@NotNull String id, Client client);
}