package com.femonres.financial.domain.services;

import com.femonres.financial.domain.entities.Client;
import com.femonres.financial.domain.repositories.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Override
    public List<Client> getAll() {
        return clientRepository.findAll();
    }

    @Override
    public Optional<Client> getOne(@NotNull String id) {
        return clientRepository.findById(UUID.fromString(id));
    }

    @Override
    public Client create(@Valid @NotNull Client client) {
        if (clientRepository.findByTaxRecord(client.getTaxRecord()).isPresent()) {
            throw new EntityExistsException("Ya existe un cliente con el mismo número de RUT");
        }

        return clientRepository.save(client);
    }

    @Override
    public Client update(@NotNull String id, Client client) {
        return null;
    }
}
