package com.femonres.financial.domain.services;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

public interface IService<D, I> {
    List<D> getAll();

    Optional<D> getOne(@NotNull I id);

    D add(@NotNull D data);

    D update(@NotNull I id, @NotNull D data);

    void delete(@NotNull I id);

}
