package com.femonres.financial.domain.services;

import com.femonres.financial.domain.entities.Account;
import com.femonres.financial.domain.entities.Movement;
import com.femonres.financial.domain.repositories.AccountRepository;
import com.femonres.financial.domain.repositories.ClientRepository;
import com.femonres.financial.domain.repositories.MovementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final ClientRepository clientRepository;

    private final AccountRepository accountRepository;

    private final MovementRepository movementRepository;

    @Override
    public Account openAccount(@NotNull String clientId, Account account) {
        if (accountRepository.findByNumber(account.getNumber()).isPresent()) {
            throw new EntityExistsException("Ya existe una cuenta registrada con el mismo número");
        }

        return clientRepository.findById(UUID.fromString(clientId))
                .map(client -> {
                    account.setClient(client);
                    return  accountRepository.save(account);
                })
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Optional<Account> getOne(@NotNull String accountId) {
        return accountRepository.findById(UUID.fromString(accountId));
    }

    @Override
    public void closeAccount(@NotNull String accountId) {
        accountRepository.findById(UUID.fromString(accountId))
                .filter(account -> account.getMovements().isEmpty())
                .ifPresentOrElse(accountRepository::delete, () -> {
                    throw new UnsupportedOperationException("La cuenta no se puede eliminar, tiene movimientos.");
                });
    }

    @Override
    public void addMovement(@NotNull String accountId, @NotNull Double amount) {
         accountRepository.findById(UUID.fromString(accountId))
                 .filter(account -> account.allowMovement(amount))
                 .map(account -> {
                     Movement newMovement = Movement.builder()
                             .account(account)
                             .currency(account.getCurrency())
                             .amount(amount)
                             .build();
                     return movementRepository.save(newMovement);
                 })
                 .orElseThrow(() -> new UnsupportedOperationException("El saldo en la cuenta es mayor al permitido"));
    }
}
