package com.femonres.financial.domain.entities;

public enum GenderEnum {
    MALE, FEMALE
}
