package com.femonres.financial.domain.entities;

public enum AccountTypeEnum {
    SAVINGS, CURRENT
}
