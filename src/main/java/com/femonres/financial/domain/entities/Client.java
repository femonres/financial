package com.femonres.financial.domain.entities;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "clients")
public class Client extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    protected UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "person_type", updatable = false, nullable = false)
    private PersonTypeEnum type;

    @NotEmpty
    @Column(name = "tax_record", unique = true, updatable = false, nullable = false)
    private String taxRecord;// RUT

    @OneToMany
    @JoinColumn(name = "accounts")
    private Set<Account> accounts;

    @Column(name = "business_name", length = 100)
    private String businessName;

    @Column(name = "foundation_year")
    private int foundationYear;

    @Column(name = "forenames", length = 80)
    private String forenames;

    @Column(name = "surnames", length = 250)
    private String surnames;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private GenderEnum gender;

    @Temporal(TemporalType.DATE)
    @Column(name = "birthday")
    private Date birthday;

    @Enumerated(EnumType.STRING)
    @Column(name = "identity_doc_type")
    private IdentityDocTypeEnum identityDocType;

    @Column(name = "identity_doc", length = 24)
    private String identityDoc;

    @Column(name = "email")
    private String email;
}
