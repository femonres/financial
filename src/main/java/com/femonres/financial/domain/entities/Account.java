package com.femonres.financial.domain.entities;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "accounts")
public class Account extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    protected UUID id;

    @Column(name = "account_number", unique = true, updatable = false, length = 22)
    private String number;

    @Enumerated(EnumType.STRING)
    @Column(name = "account_type")
    private AccountTypeEnum type;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private CurrencyEnum currency;

    @Builder.Default
    @Column(name = "balance")
    private Double balance = 0.0;

    @Builder.Default
    @Column(name = "date_opened")
    private Instant dateOpened = Instant.now();

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @OneToMany(mappedBy = "account")
    private Set<Movement> movements;

    public boolean allowMovement(Double amount) {
        switch (currency) {
            case PESO:
                return calculateBalance(amount) < 1000000;
            case DOLLAR:
                return calculateBalance(amount) < 300;
            case EURO:
                return calculateBalance(amount) < 250;
            default:
                return false;
        }
    }

    private Double calculateBalance(Double amount) {
        return (this.balance + amount);
    }
}
