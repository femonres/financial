package com.femonres.financial.domain.entities;

public enum CurrencyEnum {
    DOLLAR, EURO, PESO
}
