package com.femonres.financial.domain.entities;

public enum PersonTypeEnum {
    NATURAL, LEGAL
}
