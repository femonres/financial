package com.femonres.financial.domain.entities;

public enum IdentityDocTypeEnum {
    CC, CE, NIT
}
