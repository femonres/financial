package com.femonres.financial.domain.repositories;

import java.util.UUID;

import com.femonres.financial.domain.entities.Movement;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovementRepository extends JpaRepository<Movement, UUID> {
    
}
