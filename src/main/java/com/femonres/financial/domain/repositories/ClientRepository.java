package com.femonres.financial.domain.repositories;

import java.util.Optional;
import java.util.UUID;

import com.femonres.financial.domain.entities.Client;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ClientRepository extends JpaRepository<Client, UUID> {

    Optional<Client> findByTaxRecord(String taxRecord);
}
