package com.femonres.financial.domain.repositories;

import java.util.Optional;
import java.util.UUID;

import com.femonres.financial.domain.entities.Account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, UUID> {

    Optional<Account> findByNumber(String number);
}
