package com.femonres.financial.application.rest;

import com.femonres.financial.application.mappers.AccountMapper;
import com.femonres.financial.application.mappers.ClientMapper;
import com.femonres.financial.application.view.AccountRequest;
import com.femonres.financial.application.view.AccountResponse;
import com.femonres.financial.application.view.ClientRequest;
import com.femonres.financial.application.view.ClientResponse;
import com.femonres.financial.domain.entities.Account;
import com.femonres.financial.domain.entities.Client;
import com.femonres.financial.domain.services.AccountService;
import com.femonres.financial.domain.services.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "clients", produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientResource {

    private final ClientService clientService;

    private final AccountService accountService;

    private final ClientMapper clientMapper;

    private final AccountMapper accountMapper;


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ClientResponse>> listAllClients() {
        List<ClientResponse> allClients = clientService.getAll()
                .stream()
                .map(clientMapper::toView)
                .collect(Collectors.toList());
        return ResponseEntity.ok(allClients);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    public ResponseEntity<ClientResponse> getOne(@PathVariable String id) {
        return clientService.getOne(id)
                .map(clientMapper::toView)
                .map(ResponseEntity::ok)
                .orElseThrow(EntityNotFoundException::new);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ClientResponse> registerClient(@Valid @NotNull @RequestBody ClientRequest body) {
        Client newClient = clientService.create(clientMapper.toDomain(body));
        return ResponseEntity.status(HttpStatus.CREATED).body(clientMapper.toView(newClient));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}/accounts")
    public ResponseEntity<List<AccountResponse>> getAccountsByClient(@PathVariable String id) {
        return clientService.getOne(id)
                .map(Client::getAccounts)
                .map(accountMapper::getList)
                .map(ResponseEntity::ok)
                .orElseThrow(EntityNotFoundException::new);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/{id}/accounts")
    public ResponseEntity<AccountResponse> addAccountToClient(@PathVariable String id, @Valid @NotNull AccountRequest body) {
        Account newAccount = accountService.openAccount(id, accountMapper.toDomain(body));
        return ResponseEntity.status(HttpStatus.CREATED).body(accountMapper.toView(newAccount));
    }
}
