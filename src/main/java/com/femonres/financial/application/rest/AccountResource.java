package com.femonres.financial.application.rest;

import com.femonres.financial.application.mappers.AccountMapper;
import com.femonres.financial.application.view.AccountResponse;
import com.femonres.financial.application.view.MovementRequest;
import com.femonres.financial.domain.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "accounts", produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountResource {

    private final AccountService accountService;

    private final AccountMapper accountMapper;

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    public ResponseEntity<AccountResponse> getOne(@PathVariable String id) {
        return accountService.getOne(id)
                .map(accountMapper::toView)
                .map(ResponseEntity::ok)
                .orElseThrow(EntityNotFoundException::new);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/{id}")
    public ResponseEntity<Void> addMovement(@PathVariable String id, @RequestBody MovementRequest body) {
        accountService.addMovement(id, body.getAmount());

        return ResponseEntity.accepted().build();
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        accountService.closeAccount(id);

        return ResponseEntity.accepted().build();
    }
}
