package com.femonres.financial.application.mappers;

import com.femonres.financial.application.view.AccountRequest;
import com.femonres.financial.application.view.AccountResponse;
import com.femonres.financial.domain.entities.Account;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Mapper(componentModel = "spring")
public interface AccountMapper {

    Account toDomain(AccountRequest request);

    AccountResponse toView(Account domain);

    List<AccountResponse> getList(Set<Account> accountList);

    default UUID mapUUID(String id) {
        return id != null ? UUID.fromString(id) : null;
    }

    default String mapString(UUID id) {
        return id.toString();
    }
}
