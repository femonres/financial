package com.femonres.financial.application.mappers;

import com.femonres.financial.application.view.ClientRequest;
import com.femonres.financial.application.view.ClientResponse;
import com.femonres.financial.domain.entities.Client;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {AccountMapper.class})
public interface ClientMapper {

    Client toDomain(ClientRequest request);

    ClientResponse toView(Client domain);
}
