package com.femonres.financial.application.view;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.femonres.financial.domain.entities.Account;
import com.femonres.financial.domain.entities.GenderEnum;
import com.femonres.financial.domain.entities.IdentityDocTypeEnum;
import com.femonres.financial.domain.entities.PersonTypeEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ClientResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;

    private PersonTypeEnum type;
    private String taxRecord;

    private String businessName;

    private String forenames;
    private String surnames;
    private GenderEnum gender;
    private Date birthday;
    private IdentityDocTypeEnum identityDocType;
    private String identityDoc;
    private String email;

    @JsonIgnoreProperties({"client", "movements"})
    private List<Account> accounts;
}
