package com.femonres.financial.application.view;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class MovementRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull
    Double amount;
}
