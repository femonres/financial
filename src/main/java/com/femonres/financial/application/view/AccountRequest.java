package com.femonres.financial.application.view;

import com.femonres.financial.domain.entities.AccountTypeEnum;
import com.femonres.financial.domain.entities.CurrencyEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AccountRequest {

    @NotNull
    private String number;

    @NotNull
    private AccountTypeEnum type;

    @NotNull
    private CurrencyEnum currency;
}
