package com.femonres.financial.application.view;

import com.femonres.financial.domain.entities.GenderEnum;
import com.femonres.financial.domain.entities.IdentityDocTypeEnum;
import com.femonres.financial.domain.entities.PersonTypeEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ClientRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull
    private PersonTypeEnum type;

    @NotBlank
    private String taxRecord;

    private String businessName;
    private int foundationYear;

    private String forenames;

    private String surnames;

    private GenderEnum gender;

    @Past
    private Date birthday;

    private IdentityDocTypeEnum identityDocType;

    private String identityDoc;

    @Email
    private String email;
}
