package com.femonres.financial.application.view;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.femonres.financial.domain.entities.AccountTypeEnum;
import com.femonres.financial.domain.entities.CurrencyEnum;
import com.femonres.financial.domain.entities.Movement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AccountResponse {

    private String number;
    private AccountTypeEnum type;
    private CurrencyEnum currency;
    private Double balance;
    private Instant dateOpened;

    @JsonIgnoreProperties({"account"})
    private List<Movement> movements;
}
