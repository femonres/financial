package com.femonres.financial.domain;

import com.femonres.financial.domain.entities.Account;
import com.femonres.financial.domain.entities.CurrencyEnum;
import com.femonres.financial.domain.entities.Movement;

import java.util.Set;
import java.util.UUID;

public class MovementsProvider {
    public static Set<Movement> generateMovements(Account account, Double amount) {
        return Set.of(generateMovement(account, amount));
    }

    private static Movement generateMovement(Account account, Double amount) {
        return Movement.builder()
                .id(UUID.randomUUID())
                .account(account)
                .currency(CurrencyEnum.DOLLAR)
                .amount(amount)
                .build();
    }



}
