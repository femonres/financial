package com.femonres.financial.domain.services;

import com.femonres.financial.domain.MovementsProvider;
import com.femonres.financial.domain.entities.Account;
import com.femonres.financial.domain.AccountProvider;
import com.femonres.financial.domain.repositories.AccountRepository;
import com.femonres.financial.domain.repositories.ClientRepository;
import com.femonres.financial.domain.repositories.MovementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class AccountServiceImplTest {
    private ClientRepository clientRepository;
    private AccountRepository accountRepository;
    private MovementRepository movementRepository;
    private AccountService tested;

    @BeforeEach
    void setUp() {
        clientRepository = mock(ClientRepository.class);
        accountRepository = mock(AccountRepository.class);
        movementRepository = mock(MovementRepository.class);
        tested = new AccountServiceImpl(clientRepository, accountRepository, movementRepository);
    }

    @Test
    void allowCloseAccount() {
        Account account = spy(AccountProvider.generateAccount());
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));

        tested.closeAccount(account.getId().toString());
        verify(accountRepository).save(account);
    }

    @Test
    void deniedCloseAccount() {
        Account account = spy(AccountProvider.generateAccount());
        account.setMovements(MovementsProvider.generateMovements(account, 70.5));
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));

        final Executable executable = () -> tested.closeAccount(account.getId().toString());

        verify(accountRepository, times(0)).save(any(Account.class));
        assertThrows(RuntimeException.class, executable);
    }

    @Test
    void allowAddMovement() {
        Account account = spy(AccountProvider.generateAccount(50.5));
        account.setMovements(MovementsProvider.generateMovements(account, 70.5));
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));

        tested.addMovement(account.getId().toString(), 60.0);
        verify(accountRepository).save(account);
    }

    @Test
    void deniedAddMovement() {
        Account account = spy(AccountProvider.generateAccount(220.5));
        account.setMovements(MovementsProvider.generateMovements(account, 70.5));
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));

        final Executable executable = () -> tested.addMovement(account.getId().toString(), 60.0);

        verify(accountRepository, times(0)).save(any(Account.class));
        assertThrows(RuntimeException.class, executable);
    }
}