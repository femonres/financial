package com.femonres.financial.domain.services;

import com.femonres.financial.domain.entities.Client;
import com.femonres.financial.domain.entities.GenderEnum;
import com.femonres.financial.domain.entities.IdentityDocTypeEnum;
import com.femonres.financial.domain.entities.PersonTypeEnum;
import com.femonres.financial.domain.repositories.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class ClientServiceImplTest {
    private ClientRepository clientRepository;
    private ClientService tested;

    @BeforeEach
    void setUp() {
        clientRepository = mock(ClientRepository.class);
        tested = new ClientServiceImpl(clientRepository);
    }

    @Test
    void allowCreateClient() {
        Client client = Client.builder()
                .type(PersonTypeEnum.NATURAL)
                .taxRecord("987432")
                .forenames("Pedro")
                .surnames("Perez")
                .gender(GenderEnum.MALE)
                .identityDocType(IdentityDocTypeEnum.CC)
                .identityDoc("987432")
                .build();
        Client saveClient = tested.create(client);

        verify(clientRepository).save(any(Client.class));
        assertNotNull(saveClient.getId());
    }

    @Test
    void deniedCreateClient_incompleteFields() {
        Client client = Client.builder()
                .type(PersonTypeEnum.NATURAL)
                .forenames("Pedro")
                .build();

        final Executable executable = () -> tested.create(client);

        assertThrows(RuntimeException.class, executable);
    }

    @Test
    void deniedCreateClient_duplicatedClient() {
        Client client = Client.builder()
                .type(PersonTypeEnum.NATURAL)
                .taxRecord("987432")
                .forenames("Pedro")
                .surnames("Perez")
                .gender(GenderEnum.MALE)
                .identityDocType(IdentityDocTypeEnum.CC)
                .identityDoc("987432")
                .build();

        when(clientRepository.findByTaxRecord("987432")).thenReturn(Optional.of(client));

        final Executable executable = () -> tested.create(client);
        verify(clientRepository, times(0)).save(any(Client.class));

        assertThrows(RuntimeException.class, executable);
    }
}