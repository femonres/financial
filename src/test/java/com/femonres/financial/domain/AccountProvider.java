package com.femonres.financial.domain;

import com.femonres.financial.domain.entities.Account;
import com.femonres.financial.domain.entities.AccountTypeEnum;
import com.femonres.financial.domain.entities.Client;
import com.femonres.financial.domain.entities.CurrencyEnum;

import java.util.UUID;

public class AccountProvider {

    public static Account generateAccount() {
        return Account.builder()
                .id(UUID.randomUUID())
                .number("AC98765")
                .type(AccountTypeEnum.SAVINGS)
                .currency(CurrencyEnum.DOLLAR)
                .client(Client.builder().id(UUID.randomUUID()).build())
                .build();
    }

    public static Account generateAccount(Double balance) {
        return Account.builder()
                .id(UUID.randomUUID())
                .number("AC98765")
                .type(AccountTypeEnum.SAVINGS)
                .currency(CurrencyEnum.DOLLAR)
                .balance(balance)
                .client(Client.builder().id(UUID.randomUUID()).build())
                .build();
    }
}
