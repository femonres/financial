package com.femonres.financial.domain.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {
    private Account accountInDollars;
    private Account accountInPesos;

    @BeforeEach
    void setUp() {
        accountInDollars = Account.builder()
                .number("5432")
                .type(AccountTypeEnum.SAVINGS)
                .currency(CurrencyEnum.DOLLAR)
                .balance(200.0)
                .build();

        accountInPesos = Account.builder()
                .number("8765")
                .type(AccountTypeEnum.SAVINGS)
                .currency(CurrencyEnum.PESO)
                .balance(800000.0)
                .build();

    }

    @Test
    void allowMovementInDollars() {


        assertTrue(accountInDollars.allowMovement(80.0));
    }

    @Test
    void deniedMovementInDollars() {

        assertFalse(accountInDollars.allowMovement(160.0));
    }

    @Test
    void allowMovementInPesos() {


        assertTrue(accountInPesos.allowMovement(180000.0));
    }

    @Test
    void deniedMovementInPesos() {

        assertFalse(accountInPesos.allowMovement(260000.0));
    }
}